﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApplication3;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void TestAddQuarter()
        {
            List<Item> Items = Program.SetDefaultInventory();
            Machine OurMachine = new Machine(4, 10, 5, 0, Items);
            OurMachine.MakeDecision("4");
            Assert.AreEqual(.25, OurMachine.CustomerBalance);
        }

        [TestMethod]
        public void TestAddNickel()
        {
            List<Item> Items = Program.SetDefaultInventory();
            Machine OurMachine = new Machine(4, 10, 5, 0, Items);
            OurMachine.MakeDecision("3");
            Assert.AreEqual(.05, OurMachine.CustomerBalance);
        }

        [TestMethod]
        public void TestAddPenny()
        {
            List<Item> Items = Program.SetDefaultInventory();
            Machine OurMachine = new Machine(4, 10, 5, 0, Items);
            OurMachine.MakeDecision("2");
            Assert.AreEqual(0, OurMachine.CustomerBalance);
        }

        [TestMethod]
        public void TestAddDime()
        {
            List<Item> Items = Program.SetDefaultInventory();
            Machine OurMachine = new Machine(4, 10, 5, 0, Items);
            OurMachine.MakeDecision("1");
            Assert.AreEqual(.10, OurMachine.CustomerBalance);
        }

        [TestMethod]
        public void TestPurchaseCandy()
        {
            List<Item> Items = Program.SetDefaultInventory();
            Machine OurMachine = new Machine(4, 10, 5, 0, Items);
            //Add Quarters one by one
            OurMachine.MakeDecision("4");
            OurMachine.MakeDecision("4");
            OurMachine.MakeDecision("4");
            OurMachine.MakeDecision("4");

            //Select Canday
            OurMachine.MakeDecision("A1");
            Assert.AreEqual(2, OurMachine.GetItemInventory("A1"));
        }

        [TestMethod]
        public void TestPurchasePop()
        {
            List<Item> Items = Program.SetDefaultInventory();
            Machine OurMachine = new Machine(4, 10, 5, 0, Items);
            //Add Quarters one by one
            OurMachine.MakeDecision("4");
            OurMachine.MakeDecision("4");
            OurMachine.MakeDecision("4");
            OurMachine.MakeDecision("4");

            //Select Soda
            OurMachine.MakeDecision("A2");
            Assert.AreEqual(2, OurMachine.GetItemInventory("A2"));
        }

        [TestMethod]
        public void TestPurchaseChips()
        {
            List<Item> Items = Program.SetDefaultInventory();
            Machine OurMachine = new Machine(4, 10, 5, 0, Items);
            //Add Quarters one by one
            OurMachine.MakeDecision("4");
            OurMachine.MakeDecision("4");
            OurMachine.MakeDecision("4");
            OurMachine.MakeDecision("4");

            //Select Chips
            OurMachine.MakeDecision("A3");
            Assert.AreEqual(2, OurMachine.GetItemInventory("A3"));
        }

        [TestMethod]
        public void TestNotEnoughMoney()
        {
            List<Item> Items = Program.SetDefaultInventory();
            Machine OurMachine = new Machine(4, 10, 5, 0, Items);
            OurMachine.MakeDecision("1");
            OurMachine.MakeDecision("A1");
            Assert.AreEqual(3, OurMachine.GetItemInventory("A1"));
        }

        [TestMethod]
        public void TestCoinReturn()
        {
            List<Item> Items = Program.SetDefaultInventory();
            Machine OurMachine = new Machine(1, 1, 1, 0, Items);
            OurMachine.MakeDecision("4");
            OurMachine.MakeDecision("4");
            OurMachine.MakeDecision("1");
            OurMachine.MakeDecision("1");
            OurMachine.MakeDecision("3");

            OurMachine.MakeDecision("a3");
            Assert.AreEqual(2, OurMachine.CoinsInMachine.Count(x => x == .25));
        }

        [TestMethod]
        public void TestMustUseExactChange()
        {
            List<Item> Items = Program.SetDefaultInventory();
            Machine OurMachine = new Machine(1, 1, 1, 0, Items);

            OurMachine.MakeDecision("1");

            Assert.AreEqual(true, OurMachine.NeedsCorrectChange);
        }
    }
}
