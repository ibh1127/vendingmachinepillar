﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    /// <summary>
    /// This class represents an item that can be bought from the vending machine
    /// </summary>
    public class Item
    {
        private int quantity;
        private string location;
        private double groupCost;

        public Item(int quantity, string location, double groupCost)
        {
            this.quantity = quantity;
            this.location = location;
            this.groupCost = groupCost;
        }

        public int Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        public string GetLocation
        {
            get { return location; }
        }

        //private string group;
        public struct GroupCost
        {
            public const double Candy = .65;
            public const double Soda = 1.00;
            public const double Chips = .50;
        }

        public class PurchaseIndicator
        {
            private bool isPurchased;
            private double customerBalance;
            private double revenue;


            public PurchaseIndicator(bool isPurchased, double customerBalance, double revenue)
            {
                this.isPurchased = isPurchased;
                this.customerBalance = customerBalance;
                this.revenue = revenue;
            }

            public bool HasItemBeenPurchased
            {
                get { return isPurchased; }
            }

            public double CustomerBalance
            {
                get { return customerBalance; }
            }

            public double Revenue
            {
                get { return revenue; }
            }
        }

        public PurchaseIndicator PurchaseItem(double customerBalance)
        {
            if (quantity <= 0)
            {
                Console.WriteLine("\n SOLD OUT");
                return new PurchaseIndicator(false, customerBalance, groupCost);
            }
            else
            {
                if (customerBalance >= groupCost)
                {
                    quantity--;
                    customerBalance -= groupCost;

                    Console.WriteLine("\n THANK YOU");
                }
                else
                {
                    Console.Write("\n PRICE: ");
                    Console.Write("{0:0.00}", groupCost);
                    return new PurchaseIndicator(false, customerBalance, groupCost);
                }

                return new PurchaseIndicator(true, customerBalance, groupCost);
            }
        }
    }
}
