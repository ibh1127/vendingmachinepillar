﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    public class Program
    {
        static void Main(string[] args)
        {
            PrintMenu();

            List<Item> items = SetDefaultInventory();

            Machine OurMachine = new Machine(5, 5, 5, 0, items);

            string selection = string.Empty;

            Console.WriteLine("\n INSERT COINS\n");

            if (OurMachine.NeedsCorrectChange)
            {
                Console.WriteLine("\n EXACT CHANGE ONLY");
            }

            Console.WriteLine("\n\n\n\nQuarters in Machine: " + OurMachine.CoinsInMachine.Count(x => x == .25));
            Console.WriteLine("Nickels in Machine: " + OurMachine.CoinsInMachine.Count(x => x == .05));
            Console.WriteLine("Dimes in Machine: " + OurMachine.CoinsInMachine.Count(x => x == .10));

            while (selection.ToLower() != "off")
            {
                selection = Console.ReadLine();
                ClearConsole();
                PrintMenu();

                OurMachine.MakeDecision(selection);

                if (OurMachine.NeedsCorrectChange)
                {
                    Console.WriteLine("\n EXACT CHANGE ONLY");
                }

                Console.WriteLine("\n\n\n\nQuarters in Machine: " + OurMachine.CoinsInMachine.Count(x => x == .25));
                Console.WriteLine("Nickels in Machine: " + OurMachine.CoinsInMachine.Count(x => x == .05));
                Console.WriteLine("Dimes in Machine: " + OurMachine.CoinsInMachine.Count(x => x == .10));
            }

        }

        /// <summary>
        /// Prints the default menu displaying a list of commands
        /// </summary>
        private static void PrintMenu()
        {
            Console.WriteLine(" Commands:");
            Console.WriteLine(" 4 Insert Quarter");
            Console.WriteLine(" 1 Insert Dime");
            Console.WriteLine(" 3 Insert Nickel");
            Console.WriteLine(" 2 Insert Pennie");
            Console.WriteLine("\n A1 Select M&Ms   $0.65");
            Console.WriteLine(" A2 Select Coke     $1.00");
            Console.WriteLine(" A3 Select Doritos  $0.50");
            Console.WriteLine("\n=====================================================================");
        }

        /// <summary>
        /// Provides an default inventory for the program to work with
        /// </summary>
        /// <returns>A List of Items to purchase</returns>
        public static List<Item> SetDefaultInventory()
        {
            List<Item> items = new List<Item>();

            Item MandMs = new Item(3, "a1", Item.GroupCost.Candy);
            Item Coke = new Item(3, "a2", Item.GroupCost.Soda);
            Item Doritos = new Item(3, "a3", Item.GroupCost.Chips);

            items.Add(MandMs);
            items.Add(Coke);
            items.Add(Doritos);

            return items;
        }

        public static void ClearConsole()
        {
            Console.Clear();
        }
    }
}
