﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    public class Machine
    {
        /// <summary>
        /// Represents Balance that customer has in the machine
        /// </summary>
        private double customerBalance;

        private bool needsCorrectChange;

        public List<double> CoinsInMachine;

        private Dictionary<string, Item> inventory;

        private const string CoinReturnCommand = "cr";
        private const string InvalidCommandMessage = "\n INVALID COMMAND";
        private const string InsertCoinsMessage = "\n INSERT COINS";
        private const string CoinReturnMessage = "\n Coin Return: ";

        public double CustomerBalance
        {
            get { return customerBalance; }
            set { customerBalance = value; }
        }

        public bool NeedsCorrectChange
        {
            get { return needsCorrectChange; }
            set { needsCorrectChange = value; }
        }

        public int GetItemInventory(string location)
        {
            location = location.ToLower();
            return inventory[location].Quantity;
        }

        public Machine(int quarters, int nickels, int dimes, double customerBalance, List<Item> inventory)
        {
            this.customerBalance = customerBalance;
            this.inventory = new Dictionary<string, Item>();

            CoinsInMachine = new List<double>();
            var CoinsInMachineInt = new List<int>();

            //Add Default coins to machine
            for (int i = quarters; i != 0; i--)
            {
                CoinsInMachine.Add(.25);
            }

            for (int i = nickels; i != 0; i--)
            {
                CoinsInMachine.Add(.05);
            }

            for (int i = dimes; i != 0; i--)
            {
                CoinsInMachine.Add(.10);
            }

            foreach (var coin in CoinsInMachine)
            {
                CoinsInMachineInt.Add(Convert.ToInt32(coin * 100));
            }

            IsExactChangeNeeded(CoinsInMachineInt);

            foreach (var item in inventory)
            {
                this.inventory.Add(item.GetLocation, item);
            }
        }

        public bool IsExactChangeNeeded(List<int> CoinsInMachine)
        {
            int NumberOfSumSubsets = 0;

            //Calculates number of subsets can be made for particular change
            for (int sum = 70; sum != 0; sum -= 5)
            {
                int[] dp = new int[sum + 1];
                dp[0] = 1;
                int currentSum = 0;
                for (int i = 0; i < CoinsInMachine.Count; i++)
                {
                    currentSum += CoinsInMachine[i];
                    for (int j = Math.Min(sum, currentSum); j >= CoinsInMachine[i]; j--)
                        dp[j] += dp[j - CoinsInMachine[i]];
                }

                NumberOfSumSubsets = dp[sum];
                if (NumberOfSumSubsets == 0)
                {
                    needsCorrectChange = true;
                    break;
                }
                else
                {
                    needsCorrectChange = false;
                }
            }

            return needsCorrectChange;
        }

        /// <summary>
        /// Makes a decision based on user input of whether change is inserted, an item is selected, or there is a coin return
        /// </summary>
        /// <param name="selection"></param>
        public void MakeDecision(string selection)
        {
            selection = selection.ToLower();
            int n;

            //If selection is numeric then a coin was inserted, else it is a menu selection
            bool isNumeric = int.TryParse(selection, out n);
            if (isNumeric)
            {
                Coin InsertedCoin = new Coin();
                double coinValue = (InsertedCoin.DecipherCoin(n, n) / 100);
                if (coinValue == .01)
                {
                    Console.WriteLine(CoinReturnMessage + coinValue);
                }
                else
                {
                    InsertCoin(coinValue);
                }
                PrintCustomerBalance(customerBalance);
            }
            else
            {
                if (selection == CoinReturnCommand)
                {
                    CoinReturn();
                    PrintCustomerBalance(customerBalance);
                }
                else if (inventory.ContainsKey(selection))
                {
                    //Find Item
                    var item = inventory[selection];

                    //Purchase Item and get remaining balance
                    var purchaseIndicator = item.PurchaseItem(customerBalance);
                    customerBalance = purchaseIndicator.CustomerBalance;

                    //Transaction Complete, or sold out, not enough funds, return customer balance
                    if (customerBalance > 0 && purchaseIndicator.HasItemBeenPurchased)
                    {
                        CoinReturn();
                    }
                    else
                    {
                        PrintCustomerBalance(customerBalance);
                    }
                }
                else
                {
                    Console.WriteLine(InvalidCommandMessage);
                }
            }

            if (customerBalance == 0)
            {
                Console.WriteLine(InsertCoinsMessage);
            }

            List<int> CoinsInMachineInt = new List<int>();

            foreach (var coin in CoinsInMachine)
            {
                CoinsInMachineInt.Add(Convert.ToInt32(coin * 100));
            }

            IsExactChangeNeeded(CoinsInMachineInt);
        }

        private void InsertCoin(double coinValue)
        {
            if (customerBalance < 1)
            {
                customerBalance += coinValue;
                CoinsInMachine.Add(coinValue);
            }
            else
            {
                Console.Write(CoinReturnMessage);
                Console.Write("{0:0.00}", coinValue);
            }
        }

        private void CoinReturn()
        {
            Console.Write(CoinReturnMessage);
            Console.Write("{0:0.00}", customerBalance);

            customerBalance *= 100;
            customerBalance = Math.Round(customerBalance);
            Console.WriteLine("\n");
            while (customerBalance > 0)
            {
                if (customerBalance >= 25)
                {
                    CoinsInMachine.Remove(.25);
                    customerBalance -= 25;
                    Console.WriteLine(" Quarter Returned");
                }
                else if (customerBalance >= 10)
                {
                    CoinsInMachine.Remove(.10);
                    customerBalance -= 10;
                    Console.WriteLine(" Dime Returned");
                }
                else if (customerBalance >= 5)
                {
                    CoinsInMachine.Remove(.05);
                    customerBalance -= 5;
                    Console.WriteLine(" Nickel Returned");
                }
            }
        }


        private void PrintCustomerBalance(double customerBalance)
        {
            Console.Write("\n Current Balance: ");
            Console.Write(" $");
            Console.Write("{0:0.00}", customerBalance);
            Console.Write("\n");
        }
    }
}
