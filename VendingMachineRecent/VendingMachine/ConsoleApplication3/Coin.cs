﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    public class Coin
    {
        private enum weight { Quarter = 4, Nickel = 3, Penny = 2, Dime = 1 };
        private enum circumference { Quarter = 4, Nickel = 3, Penny = 2, Dime = 1 };
        private enum currencyValue { Quarter = 25, Nickel = 5, Penny = 1, Dime = 10 };

        public double DecipherCoin(int w, int c)
        {
            if (w == (int)weight.Quarter && c == (int)circumference.Quarter)
            {
                return (double)currencyValue.Quarter;
            }

            if (w == (int)weight.Nickel && c == (int)circumference.Nickel)
            {
                return (double)currencyValue.Nickel;
            }

            if (w == (int)weight.Penny && c == (int)circumference.Penny)
            {
                return (double)currencyValue.Penny;
            }

            if (w == (int)weight.Dime && c == (int)circumference.Dime)
            {
                return (double)currencyValue.Dime;
            }

            return 0;
        }
    }


}
