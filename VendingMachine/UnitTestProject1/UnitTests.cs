﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApplication3;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void TestAddQuarter()
        {
        }

        [TestMethod]
        public void TestAddNickel()
        {
        }

        [TestMethod]
        public void TestAddPenny()
        {
        }

        [TestMethod]
        public void TestAddDime()
        {
        }

        //TODO Following tests are not all complete
        [TestMethod]
        public void TestPurchaseCandy()
        {
        }

        [TestMethod]
        public void TestPurchasePop()
        {
        }

        [TestMethod]
        public void TestPurchaseChips()
        {
        }

        [TestMethod]
        public void TestNotEnoughMoney()
        {
        }

        [TestMethod]
        public void TestCoinReturn()
        {
        }

        [TestMethod]
        public void TestMustUseExactChange()
        {
        }
    }
}
