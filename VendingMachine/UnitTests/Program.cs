﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    [TestClass]
    class Program
    {
        [TestMethod]
        public void Test1()
        {
            // arrange
            double beginningBalance = 11.99;
            double debitAmount = 4.55;
            double expected = 1;
            // assert
            double actual = 1;
            Assert.AreEqual(expected, actual, "Account not debited correctly");
        }
    }
}
